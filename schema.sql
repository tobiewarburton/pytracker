--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: peer; Type: TABLE; Schema: public; Owner: tcw; Tablespace: 
--

CREATE TABLE peer (
    id integer NOT NULL,
    hash character(40),
    client character varying(100),
    ip character varying(16),
    key character(40),
    port integer
);


ALTER TABLE public.peer OWNER TO tcw;

--
-- Name: peer_torrent; Type: TABLE; Schema: public; Owner: tcw; Tablespace: 
--

CREATE TABLE peer_torrent (
    id integer NOT NULL,
    peer_id integer,
    torrent_id integer,
    downloaded bigint,
    uploaded bigint,
    remaining bigint,
    last_update timestamp without time zone,
    stopped boolean
);


ALTER TABLE public.peer_torrent OWNER TO tcw;

--
-- Name: torrent; Type: TABLE; Schema: public; Owner: tcw; Tablespace: 
--

CREATE TABLE torrent (
    id integer NOT NULL,
    hash character(40)
);


ALTER TABLE public.torrent OWNER TO tcw;

--
-- Name: peer_key_hash_uniq; Type: CONSTRAINT; Schema: public; Owner: tcw; Tablespace: 
--

ALTER TABLE ONLY peer
    ADD CONSTRAINT peer_key_hash_uniq UNIQUE (key, hash);


--
-- Name: peer_primary_id; Type: CONSTRAINT; Schema: public; Owner: tcw; Tablespace: 
--

ALTER TABLE ONLY peer
    ADD CONSTRAINT peer_primary_id PRIMARY KEY (id);


--
-- Name: peer_torrent_pkey_id; Type: CONSTRAINT; Schema: public; Owner: tcw; Tablespace: 
--

ALTER TABLE ONLY peer_torrent
    ADD CONSTRAINT peer_torrent_pkey_id PRIMARY KEY (id);


--
-- Name: peer_torrent_ukey_pid_tid; Type: CONSTRAINT; Schema: public; Owner: tcw; Tablespace: 
--

ALTER TABLE ONLY peer_torrent
    ADD CONSTRAINT peer_torrent_ukey_pid_tid UNIQUE (peer_id, torrent_id);


--
-- Name: torrent_hash; Type: CONSTRAINT; Schema: public; Owner: tcw; Tablespace: 
--
--
ALTER TABLE ONLY torrent
    ADD CONSTRAINT torrent_hash UNIQUE (hash);


--
-- Name: torrent_primary_id; Type: CONSTRAINT; Schema: public; Owner: tcw; Tablespace: 
--

ALTER TABLE ONLY torrent
    ADD CONSTRAINT torrent_primary_id PRIMARY KEY (id);


--
-- Name: peer_torrent_fkey_peer_id; Type: FK CONSTRAINT; Schema: public; Owner: tcw
--

ALTER TABLE ONLY peer_torrent
    ADD CONSTRAINT peer_torrent_fkey_peer_id FOREIGN KEY (peer_id) REFERENCES peer(id);


--
-- Name: peer_torrent_fkey_torrent_id; Type: FK CONSTRAINT; Schema: public; Owner: tcw
--

ALTER TABLE ONLY peer_torrent
    ADD CONSTRAINT peer_torrent_fkey_torrent_id FOREIGN KEY (torrent_id) REFERENCES torrent(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

