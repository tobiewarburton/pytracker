import web
import config
import types

from config import db

def get_all_functions(module):
    functions = {}
    for f in [module.__dict__.get(a) for a in dir(module)
        if isinstance(module.__dict__.get(a), types.FunctionType)]:
        functions[f.__name__] = f
    return functions

def sql(str):
    return web.SQLLiteral(str)

def bin2hex(str):
    return str.encode('hex')

def hex2bin(str):
    bin = ['0000','0001','0010','0011',
           '0100','0101','0110','0111',
           '1000','1001','1010','1011',
           '1100','1101','1110','1111']
    aa = ''
    for i in range(len(str)):
        aa += bin[int(str[i],base=16)]
    return aa

def set(lis, name):
    return name in lis

def grab(lis, name, default):
    if name in lis:
        return lis[name]
    else:
        return default

def store(tablename, _test=False, **values):
    try:
        db.insert(tablename, **values)
    except:
        db.update(tablename, **values)

def db_count(table, predicate, args):
    return int(db.select(table, what='COUNT(*) as db_count', where=predicate, vars=args)[0].db_count)