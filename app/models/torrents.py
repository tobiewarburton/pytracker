import web
from config import db

def get_torrent_by_id(id):
    return web.listget(db.select('torrent', vars=dict(id=id), where='id = $id'), 0, {})


def create_torrent(hash, data, name, description, file_size, file_list, owner_id):
    if does_exist(hash):
        return -1
    return db.insert('torrent', hash=hash, data=data, name=name, description=description, file_size=file_size,
        file_list=file_list, owner_id=owner_id, uploaded=web.SQLLiteral('NOW()'))


def does_exist(hash):
    return db.select('torrent', vars=dict(hash=hash), what='count(id) as c', where='hash = $hash')[
           0].c != 0


def update(id, **kw):
    db.update('torrent', vars=dict(id=id), where='id = $id', **kw)