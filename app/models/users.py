import web
import hashlib
from config import db

def create_account(username, password, email):
    db.insert('users', username=username, password=crypt(password), salt=crypt(rand_string()), email=email)


def get_user_by_name(username):
    return web.listget(db.select('users', vars=dict(username=username), where='username = $username'), 0, {})


def get_user_by_email(email):
    return web.listget(db.select('users', vars=dict(email=email), where='email = $email'), 0, {})


def is_username_available(username):
    return db.select('users', vars=dict(username=username), what='count(id) as c', where='username = $username')[
           0].c == 0


def is_valid_password(password):
    return len(password) >= 5


def is_correct_password(username, password):
    user = get_user_by_name(username)
    return crypt_password(user.get('password', False), user.get('salt', False)) == crypt_password(crypt(password),
        user.get('salt', False))


def update(id, **kw):
    db.update('users', vars=dict(id=id), where='id = $id', **kw)


def get_user_by_id(id):
    return web.listget(
        db.select('users', vars=dict(id=id),
            where='id = $id'), 0, {})


def get_user_by_key(key):
    return web.listget(
        db.select('users', vars=dict(key=key),
            where='key = $key'), 0, {})


def generate_new_key(id):
    nk = crypt_password(str(id), rand_string())
    update(id, key=nk)
    return nk


def rand_string(length=10):
    from os import urandom
    from itertools import islice, imap, repeat
    import string

    chars = set(string.ascii_uppercase + string.digits)
    char_gen = (c for c in imap(urandom, repeat(1)) if c in chars)
    return ''.join(islice(char_gen, None, length))


def crypt_password(password, salt):
    return crypt(crypt(password) + crypt(salt))


def crypt(string):
    return hashlib.md5(string).hexdigest()