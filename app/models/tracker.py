import web
from app.helpers.codec import *
from app.helpers.util import *
from config import db

def update_peer_data(_info_hash, _user_agent, _ip, _key, _port):
    # insert into db, table 'peer'
    if db_count('peer', 'hash=$_info_hash AND key=$_key', locals()) == 0:
        db.insert('peer', hash=_info_hash, key=_key, client=_user_agent, ip=_ip, port=_port)
    else:
        db.update('peer', where='hash=$_info_hash AND key=$_key', client=_user_agent, ip=_ip, port=_port, vars=locals())
    return db.select('peer', where='hash=$_info_hash AND key=$_key', vars=locals())[0]


def fetch_torrent_data(_info_hash):
    if db_count('torrent', 'hash=$_info_hash', locals()) == 0:
        return None
    return db.select('torrent', where='hash=$_info_hash', vars=locals())[0]


def update_peer_torrent_data(_pid, _tid, _dl, _ul, _rm, _stopped):
    if db_count('peer_torrent', "peer_id=$_pid AND torrent_id=$_tid", locals()) == 0:
        db.insert('peer_torrent', peer_id=_pid, torrent_id=_tid, downloaded=_dl, uploaded=_ul, remaining=_rm,
            stopped=_stopped, last_update=sql('NOW()'))
    else:
        db.update('peer_torrent', where="peer_id=$_pid AND torrent_id=$_tid", downloaded=_dl, uploaded=_ul,
            remaining=_rm, stopped=_stopped, last_update=sql('NOW()'), vars=locals())
    return db.select('peer_torrent', where='peer_id=$_pid AND torrent_id=$_tid', vars=locals())[0]


def get_seeders(_tid):
    _age = sql('(extract(epoch from now() - last_update)) <= 1800')
    return db_count('peer_torrent', 'torrent_id=$_tid AND stopped=false AND remaining=0 AND $_age', locals())


def get_leechers(_tid):
    _age = sql('(extract(epoch from now() - last_update)) <= 1800')
    return db_count('peer_torrent', 'torrent_id=$_tid AND stopped=false AND remaining>0 AND $_age', locals())


def get_peer_list(_pid, _tid, _num):
    _age = sql('(extract(epoch from now() - last_update)) <= 1800')
    return db.select('peer_torrent', where='torrent_id=$_tid AND peer_id!=$_pid AND stopped=false AND $_age',
        limit=_num, vars=locals())


def get_peer_by_id(_pid):
    return db.select('peer', where='id=$_pid', vars=locals())[0]


def build_peers(_pid, _tid, _num):
    db_list = get_peer_list(_pid, _tid, _num)
    peer_list = []
    for _peer in db_list:
        peer = get_peer_by_id(_peer.peer_id)
        peer_list.append({'peer id': hex2bin(peer.hash), 'ip': str(peer.ip), 'port': int(peer.port)})
    return peer_list
