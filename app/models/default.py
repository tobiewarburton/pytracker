import web
import app.helpers

from app.models import tracker 
from config import db

def get_stats():
    stats = []
    torrents = db.select('torrent')
    for torrent in torrents:
        meta = {}
        meta['id'] = torrent.id
        meta['name'] = torrent.name
        meta['seeders'] = tracker.get_seeders(torrent.id)
        meta['leechers'] = tracker.get_leechers(torrent.id)
        stats.append(meta)
    return stats
