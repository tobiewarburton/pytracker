import web
import config
from app.models import default
from app.helpers import *
from config import view

class index:
    def GET(self):
        return view.default_index(default.get_stats())