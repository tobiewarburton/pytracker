# Author: Alex Ksikes 

import web
from web import form

from app.models import users
from app.helpers import session

from config import view

vemail = form.regexp(r'.+@.+', 'Please enter a valid email address')

login_form = form.Form(
    form.Textbox('username', 
        form.notnull,
        description='Your username:'),
    form.Password('password', 
        form.notnull,
        description='Your password:'),
                
    form.Button('submit', type='submit', value='Login'),
    validators = [
        form.Validator('Incorrect email / password combination.', 
            lambda i: users.is_correct_password(i.username, i.password))
    ]
)

register_form = form.Form(
    form.Textbox('username', 
        form.notnull,
        form.Validator('This username is already taken.', 
        lambda x: users.is_username_available(x)),
        description='Your username:'),
    form.Password('password', 
        form.notnull,
        form.Validator('Your password must at least 5 characters long.', 
        lambda x: users.is_valid_password(x)),
        description='Choose a password:'),
    form.Textbox('email', 
        form.notnull, vemail,
        description='Your email:'),
                
    form.Button('submit', type='submit', value='Register'),
)

forgot_password_form = form.Form(
    form.Textbox('username', 
        form.notnull,
        form.Validator('There is no record of this username in our database.', 
        lambda x: not users.is_username_available(x)),
        description='Your username:'),            
    form.Button('submit', type='submit', value='Fetch'),
)

def render_account(show='all', login_form=login_form(), register_form=register_form(), forgot_password_form=forgot_password_form(), on_success_message=''):
    return view.account(show, login_form, register_form, forgot_password_form, on_success_message)

class index:
    @session.no_login_required
    def GET(self):
        return render_account(show='all')

class login:
    @session.no_login_required
    def GET(self):
        return render_account(show='login_only')
    
    @session.no_login_required
    def POST(self):
        f = self.form()
        if not f.validates(web.input(_unicode=False)):
            show = web.input(show='all').show
            return render_account(show, login_form=f)
        else:
            session.login(f.d.username)
            raise web.seeother('/')
            #raise web.seeother(session.get_last_visited_url())
    
    def form(self):
        return login_form()
    
class register:
    @session.no_login_required
    def GET(self):
        return render_account(show='register_only')
    
    @session.no_login_required
    def POST(self):
        f = self.form()
        if not f.validates(web.input(_unicode=False)):
            show = web.input(show='all').show
            return render_account(show, register_form=f)
        else:
            users.create_account(f.d.username, f.d.password, f.d.email)
            session.login(f.d.username)
            raise web.seeother('/')
    
    def form(self):
        return register_form()

class resend_password:
    @session.no_login_required
    def GET(self):
        return render_account(show='forgot_password_only')
    
    def POST(self):
        pass
    
    def form(self):
        return forgot_password_form()

class logout:
    @session.login_required
    def GET(self):
        session.logout()
        raise web.seeother(web.ctx.environ['HTTP_REFERER'])