import web
import config

from app.helpers import util
from app.helpers import codec
from app.models import tracker

def die(reason):
    return codec.bencode({'failure reason': reason})


def get_data(input):
    res = {}
    res['info_hash'] = util.bin2hex(input.get('info_hash'))
    res['peer_id'] = util.bin2hex(input.get('peer_id'))
    res['user_agent'] = web.ctx.env['HTTP_USER_AGENT'][:100]
    res['ip'] = web.ctx.env['REMOTE_ADDR']
    res['port'] = int(input.get('port'))
    res['uploaded'] = int(input.get('uploaded') or 0)
    res['downloaded'] = int(input.get('downloaded') or 0)
    res['remaining'] = int(input.get('left') or 0)
    res['numwant'] = int(input.get('numwant') or config.t_numwant)
    if res['numwant'] > config.t_max_numwant:
        res['numwant'] = config.t_max_numwant
    res['event'] = input.get('event') or ''
    res['stopped'] = res['event'] is 'stopped'
    return res


class announce:
    def GET(self, key):
        i = web.input(_unicode=False)
        if not i['info_hash'] and i['peer_id'] and  i['port']:
            return die('Invalid request')
        meta = get_data(i)
        if not meta['port'] in range(1, 65535):
            return die('Invalid client port ' + str(meta['port']))
        peer = tracker.update_peer_data(meta['info_hash'], meta['user_agent'], meta['ip'], key, meta['port'])
        torr = tracker.fetch_torrent_data(meta['info_hash'])
        if torr is None:
            return die('Torrent not registered with this tracker')
        peerid = peer.id
        torrid = torr.id
        ### TODO: seed time, free leech
        tracker.update_peer_torrent_data(peerid, torrid, meta['downloaded'], meta['uploaded'],
            meta['remaining'],
            meta['stopped'])
        if meta['stopped']:
            return codec.bencode([])
        result = {}
        result['interval'] = config.t_interval
        result['min interval'] = config.t_min_interval
        result['complete'] = tracker.get_seeders(torrid)
        result['incomplete'] = tracker.get_leechers(torrid)
        result['peers'] = tracker.build_peers(peerid, torrid, meta['numwant'])
        return codec.bencode(result)