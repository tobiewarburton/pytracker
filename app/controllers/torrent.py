import web
import pickle
import config

from web import form
from config import view

from app.models import users
from app.models import tracker
from app.models import torrents
from app.helpers import session
from app.helpers import codec

upload_form = form.Form(
    form.Textbox('name',
        form.notnull,
        description='Torrent Name:'),
    form.Textarea('description',
        form.notnull,
        description='Torrent Details:'),
    form.File('file',
        description='Torrent File:'),
    form.Button('submit', type='submit', value='Login')
)

class view_torrent:
    @session.login_required
    def GET(self, id):
        torrent = torrents.get_torrent_by_id(id)
        #if not torrent:
        #    raise web.seeother('/')
        torrent['seeders'] = tracker.get_seeders(torrent.id)
        torrent['leechers'] = tracker.get_leechers(torrent.id)
        return view.torrent_view(torrent)


class download:
    @session.login_required
    def GET(self, id):
        torrent = torrents.get_torrent_by_id(id)
        if not torrent:
            raise web.seeother('/')
        user = users.get_user_by_id(session.get_user_id())
        if not 'key' in user or user.key is '':
            key = users.generate_new_key(user.id)
        else:
            key = user.key
        print key
        torr = codec.bdecode(pickle.loads(torrent.data))
        torr['announce'] = str(config.site + '/' + key + '/announce')
        torr['comment'] = 'lolwut'
        filename = 'pyTracker-' + torrent.hash + '-' + key + '.torrent'
        web.header("Content-Disposition", "attachment; filename=%s" % filename)
        web.header("Content-Type", 'application/octet-stream')
        import io

        return io.BytesIO(codec.bencode(torr)).read()


class upload:
    def get_info_hash(self, torrent):
        import hashlib

        return hashlib.sha1(codec.bencode(torrent['info'])).hexdigest()

    def valid_file(self, name, file):
        return name.endswith(".torrent")


    def valid_torrent(self, torrent):
        required_keys = [
            'info',
            'info.piece length',
            'info.pieces',
        ]
        for key in required_keys:
            if '.' in key:
                #sub dict
                return key.split('.')[1] in torrent[key.split('.')[0]]
            else:
                if not key in torrent:
                    return False
                    # now check if single/multi file
        info = torrent['info']
        multi = 'files' in info
        if not 'file' in info and not multi:
            return False
        if multi:
            for f in info['files']:
                if not 'length' in f or not 'path' in f:
                    return False
        if not 'length' in info['file'] or not 'name' in info['file']:
            return False
        return True


    def fix_torrent(self, torrent):
        # strip un needed data
        torrent['info']['private'] = 1
        remove = ['url-list',
                  'announce-list',
                  'creation date',
                  'azureus_properties',
                  'nodes',
                  'comment',
                  'created by',
                  'encoding',
                  'announce']
        for r in remove:
            if r in torrent:
                del torrent[r]
        return torrent

    def get_files(self, torrent):
        length = 0
        files = [] # name, length
        if not 'files' in torrent['info']: # single file
            length += int(torrent['info']['length'])
            files.append(torrent['info']['name'])
        else:
            for f in torrent['info']['files']:
                length += int(f['length'])
                files.append(f['path'])

        return [length, files]


    @session.login_required
    def GET(self):
        form = upload_form()
        return view.torrent_upload(form)


    @session.login_required
    def POST(self):
        form = upload_form()
        i = web.input(file={}, _unicode=False)
        if not form.validates(i):
            form.fill(i)
            return view.torrent_upload(form)
        if not i.file.filename.endswith('.torrent'):
            return view.torrent_upload(form)

        try:
            torrent = codec.bdecode(i.file.value)
        except:
            del i.file
            form.fill(i)
            return view.torrent_upload(form)

        if not self.valid_torrent(torrent):
            print 'not valid'
            return view.torrent_upload(form)

        torrent = self.fix_torrent(torrent)

        info_hash = self.get_info_hash(torrent)
        files = self.get_files(torrent)
        torrent_data = pickle.dumps(
            codec.bencode(torrent)) # using pickle just to be sure it's safe to enter to the database
        #hash, data, name, description, file_size, file_list, owner_id
        id = torrents.create_torrent(info_hash, torrent_data, i.name, i.description, files[0], ';'.join(files[1]),
            session.get_user_id())

        raise web.seeother('/')