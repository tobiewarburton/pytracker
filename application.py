import web
import config
import app.controllers

from app.helpers import session

urls = (
    '/public/.+',                                'app.controllers.public.public',

    '/',                                         'app.controllers.default.index',

    '/account',                                  'app.controllers.account.index',
    '/account/register',                         'app.controllers.account.register',
    '/account/login',                            'app.controllers.account.login',
    '/account/logout',                           'app.controllers.account.logout',
    '/account/resend_password',                  'app.controllers.account.resend_password',

    '/torrent/(\d+)',                            'app.controllers.torrent.view_torrent',
    '/torrent/download/(\d+)',                   'app.controllers.torrent.download',
    '/torrent/upload',                           'app.controllers.torrent.upload',

    '/(.+)/announce',                            'app.controllers.tracker.announce',
)

app = web.application(urls, globals())
session.add_sessions_to_app(app)

if __name__ == "__main__":
    app.run()