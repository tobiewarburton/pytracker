import web, os

# connect to database
db = web.database(dbn='postgres', user='tcw', pw='', db='tracker')

from app.helpers import misc
from app.helpers import util

# in development debug error messages and reloader
web.config.debug = True

# in develpment template caching is set to false
cache = False

site = 'http://127.0.0.1:8080'

# global used template functions
globals = util.get_all_functions(misc)
globals['site'] = site

# set global base template
view = web.template.render('app/views/', base='layout', cache=cache, globals=globals)

t_numwant = 30 # default amount of peers to push
t_max_numwant = 50 # max amount of peers to push

t_interval = 1800 # interval in seconds
t_min_interval = 60 # interval in seconds
